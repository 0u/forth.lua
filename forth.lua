local forth = {}
local stack = require('stack')
local io = require('bufio')

-- fn = function(x, y) -> int
local two_arg = function(fn)
  return function(stack)
    local x, y = stack:pop(), stack:pop()
    stack:push(fn(y, x))
  end
end

-- fn = function(x) -> int
local one_arg = function(fn)
  return function(stack)
    stack:push(fn(stack:pop()))
  end
end

-- fn = function(x, y) -> bool
local bool_fn = function(fn)
  return function(stack)
    local x, y = stack:pop(2)
    if fn(x, y) then
      stack:push(-1) -- true
    else
      stack:push(0) -- false
    end
  end
end

-- Forth default words.
local words = {
  -- Conditionals
  ["="] = bool_fn(function(x, y) return x == y end),
  [">"] = bool_fn(function(x, y) return x > y end),
  ["<"] = bool_fn(function(x, y) return x < y end),

  -- Math
  ["+"]   = two_arg(function(x, y) return x + y end),
  ["-"]   = two_arg(function(x, y) return x - y end),
  ["*"]   = two_arg(function(x, y) return x * y end),
  ["/"]   = two_arg(function(x, y) return math.floor(x / y) end),
  ["mod"] = two_arg(function(x, y) return x % y end),
  ["negate"] = function(stack)
    stack:push(-stack:pop())
  end,
  [".s"] = function(stack, io)
    io:write('<' .. tostring(#stack) .. '>')
    local i = 1
    local x = stack[i]
    while x ~= nil do
      io:write(' ' .. tostring(x))
      i = i + 1
      x = stack[i]
    end
  end,
  ["abs"] = one_arg(math.abs),
  ["max"] = two_arg(math.max),
  ["min"] = two_arg(math.min),
  ["."] = function(stack, io)
    io:write(stack:pop())
  end,

  ["dup"] = function(s) s:push(s:peek()) end,
  ["swap"] = function(s)
    local x,y = s:pop(2)
    s:push(y)
    s:push(x)
  end,
  -- rotate top 3
  -- 1 2 3 -> 2 3 1
  -- 6 4 5 -> 4 5 6
  ["rot"] = function(s)
    local a,b,c = s:pop(3)
    s:push(b,c,a)
  end,
  ["drop"] = function(s) s:pop() end,
  ["nip"] = function(s)
    local v = s:pop()
    s:pop()
    s:push(v)
  end,

  ["tuck"] = function(s)
    local mid, top = s:pop(2)
    s:push(top, mid, top)
  end,

  -- duplicate the second item to the top
  ["over"] = function(s)
    local second, top = s:pop(2)
    s:push(second, top, second)
  end,

  -- *move* the item at that position to the top
  -- (1 2 3 4 2 roll) want "1 3 4 2", got "1 2 3 4 2"
  ["roll"] = function(s)
    local position = s:pop()
    local item = s[position]
    table.remove(s, position)
    s:push(item)
  end,

  -- *duplicate* the item at that position to the top
  ["pick"] = function(s)
    local position = s:pop()
    local item = s[position]
    s:push(item)
  end,
}

words.__index = words

function words.new()
  local self = setmetatable({}, words)
  return self
end

-- Psudocode for the type of a forth interpreter state,
-- created by forth.new().
-- struct state {
--   stack = list<Literal>,
--   words  = table<string -> function>,
--   compile = bool,
--   compile_buf = {token},
--   see = bool,
--   quote = bool,
-- }

-- Create a forth interpreter.
function forth.new()
  return setmetatable({
      stack = stack.new(),
      words = words.new(),
      io = io.new(),
    }, { __index = forth })
end

-- Read a literal token. returns nil if it cannot be read.
function forth.read_lit(x)
  return tonumber(x)
end

-- Evaluate a single forth token.
function forth:eval_token(token)
  if token == ':' then -- begin compiling
    self.compile_buf = stack.new() -- using stack for ease of use.
    self.compile = true

  elseif token == ';' then -- stop compiling
    self.compile = false
    local name = self.compile_buf[1]
    table.remove(self.compile_buf, 1)

    self.words[name] = self.compile_buf

  elseif self.compile then -- compile mode
    table.insert(self.compile_buf, token)

  elseif token == 'see' then -- begin parsing a 'see' statement
    self.see = true

  elseif self.see then -- continue parsing a 'see' statement
    self.io:write(': ' .. token .. ' ')
    for _, tok in ipairs(self.words[token]) do
      self.io:write(tok .. ' ')
    end
    self.io:write(';')
    self.see = false

  elseif token == '."' then -- begin parsing a string
    self.quote = true

  elseif self.quote then -- continue parsing inside quotes
    if token:sub(#token, #token) == '"' then
      token = token:sub(1, #token-1)
      self.quote = false
      self.io:write(token)
    else
      self.io:write(token .. ' ')
    end

  elseif self.words[token] then -- is it a word?
    local word = self.words[token]
    if type(word) == 'function' then
      word(self.stack, self.io)
    elseif type(word) == 'table' then
      for _, token in ipairs(word) do
        self:eval_token(token)
      end
    end

  elseif forth.read_lit(token) then -- is it a literal?
    self.stack:push(forth.read_lit(token))
  else
    error(('Undefined word %q'):format(token))
  end
end

-- Evaluate some forth code with a stack, and return the resulting stack.
-- note: This should be called for each line in a file.
function forth:eval(str)
  -- TODO: Define comments in the language :))
  str = str:gsub("%\\[^\n\r]+", "")
  str = str:gsub("%([^\n\r]+%)", "")

  local tokens = str:gmatch("%S+")
  for token in tokens do
    self:eval_token(token)
  end

  -- ugly will fix later
  if self.io.buf:sub(#self.io.buf, #self.io.buf) == '' then
    self.io:write('ok')
  else
    self.io:write(' ok')
  end
end

return forth
