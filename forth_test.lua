local forth = require('forth')
local stack = require('stack')
local bufio = require('bufio')

local test = {}

---------------------- Test helpers ----------------------
local assert_stack = function(stack, want, code)
  local s = tostring(stack)
  local w = stack.__tostring(want)

  assert(s == w, ('(%s) want %q, got %q'):format(code, w, s))
end

local assert_eval = function(str, stack, f)
  if not f then
    f = forth.new()
  end
  return function()
    f:eval(str)

    assert_stack(f.stack, stack, str)
  end
end

local assert_evals = function(t, f)
  return function()
    local i = 1
    while t[i] and t[i+1] do
      assert_eval(t[i], t[i+1], f)()
      i = i + 2
    end
  end
end

local assert_words = function(t)
  return function()
    local i = 1
    while t[i] and t[i+1] do
      local f = forth.new()
      f:eval(t[i]) -- define the word

      -- test the word is working as expected.
      assert_evals(t[i+1], f)
      i = i + 2
    end
  end
end

--------------------------- Test the stack implementation ---------------------------
function test.stack()
  -- TODO: Remove duplication.
  do
    local s = stack.new()
    s:push(1,2,3)
    assert(tostring(s) == '1 2 3')

    local a,b = s:pop(2)
    local got = tostring(a) .. ' ' .. tostring(b)
    local want = '2 3'
    assert(got == want, ('want %q got %q'):format(want, got))
  end

  do
    local s = stack.new()
    s:push(1,2,3)
    assert(tostring(s) == '1 2 3')

    local a,b,c = s:pop(3)
    local got = tostring(a) .. ' ' .. tostring(b) .. ' ' .. tostring(c)
    local want = '1 2 3'
    assert(got == want, ('want %q got %q'):format(want, got))
  end
end


--------------------------- Test the bufio implementation ---------------------------
function test.bufio()
  local x = bufio.new()
  x:write(1, 2)
  assert(x.buf == '1\t2')

  x.buf = ''
  x:print(1, 2)
  assert(x.buf == '1\t2\n')
end

----------------------------------- Errors -----------------------------------
function test.underflow()
  local f = forth.new()
  local ok, err = pcall(function()
    f:eval('+')
  end)

  assert(not ok, 'Expected stack underflow')
end

function test.undefined()
  local f = forth.new()
  local ok, err = pcall(function()
    f:eval('asd')
  end)

  assert(not ok, 'Expected undefined word')
end

----------------------------------- Basic math -----------------------------------
test.add = assert_eval('10 20 +', {30})
test.sub = assert_eval('4 2 -', {2})
test.mul = assert_eval('4 2 *', {8})
test.div = assert_eval('4 2 /', {2})
test.mod = assert_eval('4 2 mod', {0})
test.negate = assert_evals {
  '4 negate', {-4},
  '4 2 negate', {4, -2},
}
test.abs = assert_eval('-99 abs', {99})

-- Max and min
test.max = assert_eval('10 20 max', {20})
test.min = assert_eval('10 20 min', {10})

-- Test that add pops two items off the stack.
test.add_takes_two = assert_eval('10 20 30 +', {10, 50})

-- Test with many operators
test.many_operators = assert_eval('10 20 30 40 50 + - + +', {-30})

-- Test popping from the stack
test.pop = assert_evals {
  '10 .', {},
  '10 20 .', {10},
}

----------------------------- Stack Manipulation -----------------------------

test.stack_manip = assert_evals {
  '3 dup', {3, 3},        -- duplicate the top item (1st now equals 2nd)
  '2 5 swap', {5, 2},     -- swap the top with the second element
  '6 4 5 rot', {4, 5, 6}, -- rotate the top 3 elements
  '4 0 drop', {4},        -- remove the top item (don't print to screen)
  '1 2 3 nip', {1, 3},    -- remove the second item (similar to drop)
}

---------------------- More Advanced Stack Manipulation ----------------------

test.stack_manip_advanced = assert_evals {
  '1 2 3 4 tuck',   {1, 2, 4, 3, 4}, -- duplicate the top item below the second slot:
  '1 2 3 4 over',   {1, 2, 3, 4, 3}, -- duplicate the second item to the top:
  '1 2 3 4 2 roll', {1, 3, 4, 2},    -- *move* the item at that position to the top:
  '0 1 2 3 2 pick', {0, 1, 2, 3, 1}, -- *duplicate* the item at that position to the top:
}

------------------------------ Creating Words ------------------------------

test.words = assert_words {
  ': push10 10 ;', {
    'push10', {10},
  },

  ': square dup * ;', {
    '10 square', {100},
    '5 square',  {25},
  },
}

------------------------------ Interpreter output ------------------------------

function test.output()
  local tests = io.open('forth_test.fth')
  local f = forth.new()

  for test in tests:lines() do
    local want = test:gmatch("%\\[^\n\r]+")()
    want = want:sub(3, #want)
    local ok, err = pcall(function()
      f:eval(test)
    end)
    if not ok then
      error(('want %q, got %q'):format(want, err))
    end

    assert(want == f.io.buf, ('want %q, got %q'):format(want, f.io.buf))
    f.io.buf = ''
  end
end

local pad = 25
for name, tc in pairs(test) do
  io.write('TEST ' .. name .. (' '):rep(pad-name:len()) .. '| ')
  local ok, err = pcall(tc)
  print(err or 'OK')
end

