local stack = {}
stack.__index = stack

function stack:__tostring()
  local s = ''
  for i,v in ipairs(self) do
    s = s .. tostring(v) .. ' '
  end
  return s:sub(1, #s-1)
end

function stack:push(...)
  for _,v in ipairs{...} do
    table.insert(self, v)
  end
  return self
end

function stack:pop(n)
  n = n or 1
  local ret = {}

  for i=n,1,-1 do
    local v = self:peek()
    table.remove(self, #self)
    ret[i] = v
  end

  return table.unpack(ret)
end

function stack:peek()
  local v = self[#self]
  if v == nil then
    error('Stack underflow')
  end
  return v
end

function stack.new()
  local s = {}
  return setmetatable(s, stack)
end

return stack
