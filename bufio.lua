-- Buffered IO implementation.
local IO = {}

function IO:flush()
  io.write(self.buf)
  self.buf = ''
end

function IO:write(...)
  local s = ''
  for i,v in ipairs{...} do s = s .. tostring(v) .. '\t' end
  self.buf = self.buf .. s:sub(1, #s-1)
end

function IO:print(...)
  self:write(...)
  self.buf = self.buf .. '\n'
end

function new()
  local t = { buf = '' }
  setmetatable(t, { __index = IO })
  return t
end

return { new = new }
