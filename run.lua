local forth = require('forth')

local f = forth.new()
for line in io.stdin:lines() do
  f:eval(line)
  f.io:print()
  f.io:flush()
end

